const fetch = require('node-fetch')

var data_center = "us10"
var api_key = "5f4a699913aa3aa62153dc59b7152dc4-us10"
var list_id = "831347e638"

var body = {
    "email_address": "urist.mcvankab@freddiesjokes.com",
    "status": "subscribed",
    "merge_fields": {
        "FNAME": "Urist",
        "LNAME": "McVankab"
    }
}

fetch(`https://${data_center}.api.mailchimp.com/3.0/lists/${list_id}/members`, {
	method: "POST", 
	headers: {
		"Content-type": "application/json",
		"Authorization": `Basic ${api_key}`
	},
	body: JSON.stringify(body)
})
.then(res => res.json())
.then(data => {
		console.log(data)
})