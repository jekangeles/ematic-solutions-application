const fetch = require('node-fetch')

var data_center = "us10"
var api_key = "5f4a699913aa3aa62153dc59b7152dc4-us10"

var body = {
	"name": "My Sample Newsletter",
	"contact": {
		"company": "Sample Company, Inc.",
		"address1": "Unit 35 Sample Bldg., Sample Blvd",
		"address2": "",
		"city": "Example City",
		"state": "Metro Manila",
		"zip": "1055",
		"country": "Philippines",
		"phone": "+639176532732"
	},
	"permission_reminder": "You are receiving this email because you subscribed to Sample Company, Inc. - Newsletter",
	"use_archive_bar": true,
	"campaign_defaults": {
		"from_name": "Sample",
		"from_email": "news@sampleinc.com",
		"subject": "",
		"language": "en"
	},
	"notify_on_subscribe": "",
	"notify_on_unsubscribe": "",
	"email_type_option": false,
	"visibility": "prv",
	"double_optin": false,
	"marketing_permissions": false
}

fetch(`https://${data_center}.api.mailchimp.com/3.0/lists/`, {
	method: "POST",
	headers: {
		"Authorization": `Basic ${api_key}`
	},
	body: JSON.stringify(body)
})
.then(res => res.json())
.then(data => {
	if(data.status === 200) {
		console.log('Success! The ff has been added:')	
		console.log(data)
	} else {
		console.log(data)
	}
})